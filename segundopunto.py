from pyfirmata import Arduino, util
from tkinter import *
from tkinter import messagebox
from PIL import Image , ImageTk  #libreria manejo imagenes
from time import sleep

window_width=1920
window_height=1080
canvas_width =  1900
canvas_height = 980
sizex_circ=60
sizey_circ=60
width = 100
height = 70



placa = Arduino ('COM4')
it = util.Iterator(placa)
#inicio el iteratodr
it.start()
led1 = placa.get_pin('d:8:o') 
led2 = placa.get_pin('d:9:o') 
led3 = placa.get_pin('d:10:o') 
led4 = placa.get_pin('d:11:o') 
led5 = placa.get_pin('d:12:o') 
led6 = placa.get_pin('d:13:o') 
ventana = Tk()
ventana.state('zoomed') #iniciar la pantalla maximizada
ventana.configure(bg = 'white')
ventana.title("Repaso")
lienzo = Canvas(ventana, width=canvas_width, height=canvas_height, bg="lavender")
lienzo.place(x =10,y = 10)
texto = Label(lienzo, text="Ejercicio 2 del  parcial", bg='maroon4', font=("Berlin Sans FB", 20), fg="white")
texto.place(x=850, y=20)

jose = Label(lienzo, text="", bg='lavender', font=("Berlin Sans FB", 20), fg="white")
jose.place(x=165, y=110)

luis = Label(lienzo, text="", bg='lavender', font=("Berlin Sans FB", 20), fg="white")
luis.place(x=165, y=210)

hola = Label(lienzo, text="", bg='lavender', font=("Berlin Sans FB", 20), fg="white")
hola.place(x=165, y=310)

hola1 = Label(lienzo, text="", bg='lavender', font=("Berlin Sans FB", 20), fg="white")
hola1.place(x=165, y=410)

hola2 = Label(lienzo, text="", bg='lavender', font=("Berlin Sans FB", 20), fg="white")
hola2.place(x=165, y=510)

hola3 = Label(lienzo, text="", bg='lavender', font=("Berlin Sans FB", 20), fg="white")
hola3.place(x=165, y=610)


img = Image.open("C:/Users/rodri/Desktop/tkinter/logousa.png") 



# abro la imagen
img = img.resize((width,height)) #se cambia el tamaño
imagen =  ImageTk.PhotoImage(img)  #almaceno la imagen en un objeto
Label(lienzo,image=imagen).place(x = 10 ,y = 10) #coloco la imagen en un objeto label
dibujo_led1=lienzo.create_oval(50,100,100+sizex_circ,100+sizey_circ,fill="white")
dibujo_led2=lienzo.create_oval(50,200,100+sizex_circ,200+sizey_circ,fill="white")
dibujo_led3=lienzo.create_oval(50,300,100+sizex_circ,300+sizey_circ,fill="white")
dibujo_led4=lienzo.create_oval(50,400,100+sizex_circ,400+sizey_circ,fill="white")
dibujo_led5=lienzo.create_oval(50,500,100+sizex_circ,500+sizey_circ,fill="white")
dibujo_led6=lienzo.create_oval(50,600,100+sizex_circ,600+sizey_circ,fill="white")

def intensidad(dato_lum):
    dato_lum=int(dato_lum)
    if(dato_lum>0):
        lienzo.itemconfig(dibujo_led1, fill="gold")
        jose.configure(text="led8", bg="gold")
        led1.write(1)
    else:
        lienzo.itemconfig(dibujo_led1, fill="white")
        jose.configure(text="", bg="lavender")
        led1.write(0)

    if(dato_lum>16 and dato_lum<=33):
        lienzo.itemconfig(dibujo_led2, fill="red")
        luis.configure(text="led9", bg="red")
        led2.write(1)
    if(dato_lum<16):
        lienzo.itemconfig(dibujo_led2, fill="white")
        luis.configure(text="", bg="lavender")
        led2.write(0)
   
    if(dato_lum>33 and dato_lum<=50):
        lienzo.itemconfig(dibujo_led3, fill="deep pink")
        hola.configure(text="led10", bg="deep pink")
        led3.write(1)
        
    if(dato_lum<33):
        lienzo.itemconfig(dibujo_led3, fill="white")
        hola.configure(text="", bg="lavender")
        led3.write(0)
    if(dato_lum>50 and dato_lum<=66):
        lienzo.itemconfig(dibujo_led4, fill="SkyBlue1")
        hola1.configure(text="led11", bg="SkyBlue1")
        led4.write(1)
    if (dato_lum<50):
        lienzo.itemconfig(dibujo_led4, fill="white")
        hola1.configure(text="", bg="lavender")
        led4.write(0)
    if (dato_lum>66 and dato_lum<=82) :
        lienzo.itemconfig(dibujo_led5, fill="green2")
        hola2.configure(text="led12", bg="green2")
        led5.write(1)

    if (dato_lum<66):
        lienzo.itemconfig(dibujo_led5, fill="white")
        hola2.configure(text="", bg="lavender")
        led5.write(0)


    if (dato_lum>82 and dato_lum<=100) :
        lienzo.itemconfig(dibujo_led6, fill="yellow green")
        hola3.configure(text="led13", bg="yellow green")
        led6.write(1)
    if (dato_lum<82):
        lienzo.itemconfig(dibujo_led6, fill="white")
        hola3.configure(text="", bg="lavender")
        led6.write(0)

    
    


lum = Scale(ventana, 
            command=intensidad, 
            from_= 0, 
            to = 100, 
            orient = HORIZONTAL , 
            length =200 , 
            label = 'Regular Intensidad',
            bg = 'purple4',
            font=('Helvetica',12 ),
            fg="red"

            )





lum.place(x=425 ,y=70 )

ventana.mainloop()

